
# Sign Language Translator React

Sumbission for Assignment 2 "Create a sign language translator using React"

As per Appendix A the webpage for translating words was created. 
All functionality and documentation required has been met. Details bellow. 

# Summary

The aim of the assignment was to create a webpage using React where you can login using a username then input the words you want translated
ininto sign language and save the history of your searches in an API.

# What we used

We used heroku so as to save our information and Visual Studio Code along with React to develop the webpage

# What we learned

Working with React was a new and fresh experience. As all of us have a backend background it was challenging but also very interesting to 
use React alongside HTML,CSS and Javascript to create a webpage that has a great functionality but also is good looking as well 

#### App
- Download/Clone the repo. 
- Open the solution in your Visual Studio Code environment.
- Run the application with npm start
- Depending on which API you want to work you need to change the URL and the KEY in the .env file

# Hosting Platform ( Vercel ) 
[](https://assignment5-translation-app-giatrip.vercel.app/)

## Authors / Contributors

- **Aris Kardasis** [@ArisKard](https://github.com/@ArisKard) (Aris Kardasis on moodle)
- **Giannis Tripodis** [@giatrip](https://gitlab.com/giatrip) (Giannis Tripodis on moodle)
- **Bill Koutsis** [@Vasikout](https://www.github.com/@Vasikout) (Vasilis Koutsis on Moodle)
