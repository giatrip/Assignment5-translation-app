import { NavLink } from "react-router-dom"
import { useUser } from "../../context/UserContext"
import { storageDelete} from "../../utils/storage"
import { Link } from "react-router-dom"
import { STORAGE_KEY_USER } from "../../const/storageKeys"

const Navbar = () => {

    const { user, setUser } = useUser()

    const handleLogoutClick = () =>{
        if (window.confirm('Are you sure?')){
            storageDelete(STORAGE_KEY_USER)
            setUser(null)
        }
    }

    return (
        <nav className="nav-bar" >
                <h2>Lost in Translation</h2>
            { user !==null &&
                <ul className="options">
                    <ul>
                        <NavLink to="/translations">Translate</NavLink>
                    </ul>
                    <ul>
                        <NavLink to="/profile">Profile</NavLink>
                    </ul>
                    <ul>
                        <button className="button-logout" onClick={ handleLogoutClick }>Log out</button>
                    </ul>  
                </ul>
            }
        </nav>
    )
}
export default Navbar