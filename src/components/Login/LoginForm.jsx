import { useState, useEffect } from "react"
import { useForm } from "react-hook-form"
import { loginUser } from '../../api/user'
import { storageSave } from "../../utils/storage"
import { useNavigate } from 'react-router-dom'
import { useUser } from "../../context/UserContext"
import { STORAGE_KEY_USER } from "../../const/storageKeys"

const usernameConfig = {
    required: true,
    minLength: 3
}


const LoginForm = () => {
    // Hooks
    const { register, handleSubmit, formState: { errors } } = useForm()
    const { user, setUser } = useUser()
    const navigate = useNavigate()

    // Local state
    const [loading, setLoading] = useState(false)
    const [apiError, setApiError] = useState(null)

    // Side Effects
    useEffect(() => {
        if (user !== null) {
            navigate('/profile')
        }
    }, [user, navigate]) // Empty Deps - Only run 1ce

    // Event Handlers
    const onSubmit = async ({ username }) => {
        setLoading(true)
        const [error, userResponse] = await loginUser(username)
        if (error !== null) {
            setApiError(error)
        }
        if (userResponse !== null) {
            storageSave(STORAGE_KEY_USER, userResponse)
            setUser(userResponse)
        }
        setLoading(false)
    }

    // Render Functions
    const errorMessage = (() => {
        if (!errors.username) {
            return null
        }
        if (errors.username.type === 'required') {
            return <span>Username is required</span>
        }
        if (errors.username.type === 'minLength') {
            return <span>Username must be at least 3 characters</span>
        }

    })()


    return (
        <>
            <div className="login-form-container">
                <form onSubmit={handleSubmit(onSubmit)}>
                    <fieldset>
                        <img src="keyboard.png" htmlFor="username" className="keyboard-image"></img>
                        <input
                            type="text"
                            placeholder="Username"
                            {...register("username", usernameConfig)}
                        />
                        <button className="button-login" type="submit" disabled={loading}> <img src="forward_arrow.png" height="30px"></img> </button>
                    </fieldset>

                    {/* <button type="submit" disabled={loading}>Continue</button> */}

                </form>
                    {errorMessage}

                    {loading && <p>Logging in ...</p>}
                    {apiError && <p>{apiError}</p>}
            </div>
        </>
    )
}
export default LoginForm