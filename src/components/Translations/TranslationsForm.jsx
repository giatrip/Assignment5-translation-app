import { useForm } from "react-hook-form"

const TranslationsForm = ({ onTranslate }) => {

    const { register, handleSubmit} = useForm()

    const onSubmit = ({ textToTranslate }) => {
        textToTranslate = textToTranslate.replace(/[^a-zA-Z ]/g, "") // numbers and symbols are ignored
        if (textToTranslate.length < 1) {
            alert("Empty string!\n(Numbers and symbols get ignored)")
            return
        }
        if (textToTranslate.length > 40) {
            alert("Cant exceed 40 lettes")
            return
        }
        onTranslate(textToTranslate)
    }


    return (
        <div className="translate-input-container">
            <form onSubmit={handleSubmit(onSubmit)}>
                <fieldset>
                    <input type="text" placeholder="text to translate"{...register('textToTranslate')} />
                    <button className="button-login"type="submit"> <img src="forward_arrow.png" height="20px" ></img></button>
                </fieldset>
            </form>
        </div>
    )
}
export default TranslationsForm