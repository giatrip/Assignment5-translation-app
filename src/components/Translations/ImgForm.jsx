import {useForm } from "react-hook-form"
import { useEffect, useState } from "react"

const ImgForm = ({ myText }) =>{

    const [ myPaths , setMyPaths ] = useState([])

    useEffect( () => {
        setMyPaths(translatePhrase(myText))
    } , [myText])

    const ICONS = [
    {
        name:"a",
        image:"icons/a.png"
    },
    {
        name:"b",
        image:"icons/b.png"
    },
    {
        name:"c",
        image:"icons/c.png"
    },
    {
        name:"d",
        image:"icons/d.png"
    },
    {
        name:"e",
        image:"icons/e.png"
    },
    {
        name:"f",
        image:"icons/f.png"
    },
    {
        name:"g",
        image:"icons/g.png"
    },
    {
        name:"h",
        image:"icons/h.png"
    },
    {
        name:"i",
        image:"icons/i.png"
    },
    {
        name:"j",
        image:"icons/j.png"
    },
    {
        name:"k",
        image:"icons/k.png"
    },
    {
        name:"l",
        image:"icons/l.png"
    },
    {
        name:"m",
        image:"icons/m.png"
    },
    {
        name:"n",
        image:"icons/n.png"
    },
    {
        name:"o",
        image:"icons/o.png"
    },
    {
        name:"p",
        image:"icons/p.png"
    },
    {
        name:"q",
        image:"icons/q.png"
    },
    {
        name:"r",
        image:"icons/r.png"
    },
    {
        name:"s",
        image:"icons/s.png"
    },
    {
        name:"t",
        image:"icons/t.png"
    },
    {
        name:"u",
        image:"icons/u.png"
    },
    {
        name:"v",
        image:"icons/v.png"
    },
    {
        name:"w",
        image:"icons/w.png"
    },
    {
        name:"x",
        image:"icons/x.png"
    },
    {
        name:"y",
        image:"icons/y.png"
    },
    {
        name:"z",
        image:"icons/z.png"
    }
    ]

    const translateLetter = (letter) => {
        for ( let i in ICONS ){
            if ( letter === ICONS[i].name ){
                return (ICONS[i].image)
            }
        }
    }

    const  translatePhrase = ( phrase ) => {
        let listOfLetters = phrase.split("")
        let listOfIconsPath = listOfLetters.map(x => translateLetter(x.toLowerCase()))
       // listOfIconsPath=phrase.split("").map(x => translateLetter(x))
        return  listOfIconsPath
    }

    

    return (
       <div>
                {
                    myPaths.map((x , index) => (
                        <img key={ index } src={ x } height="80px"></img>
                    ))
                }
                
       </div>
    )
}
export default ImgForm