
const ProfileHeader = ({ username }) => {
    return (
        <header>
            <h3> Welcome { username }</h3>
        </header>
    )
}
export default ProfileHeader