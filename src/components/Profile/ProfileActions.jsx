
import { useUser } from "../../context/UserContext"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { storageSave } from "../../utils/storage"
import { translationClearHistory } from "../../api/translation"

const ProfileActions = () => {

    const { user, setUser } = useUser()

    const handleClearHistoryClick = async () =>{
        if (!window.confirm('Are you sure?\nThis can not be undone!')){
            return
        }

        const [ clearError ] = await translationClearHistory(user.id)

        if (clearError !== null){
            return
        }

        const updatedUser = {
            ...user,
            translations: []
        }

        storageSave(STORAGE_KEY_USER,updatedUser)
        setUser(updatedUser)
    }

    return (
            <ul><button  className="clear-history" onClick={ handleClearHistoryClick }>Clear history</button></ul>
    )
}
export default ProfileActions