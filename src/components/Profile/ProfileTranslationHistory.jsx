import ProfileActions from "./ProfileActions"
import ProfileTranslationHistoryItem from "./ProfileTranslationHistoryItem"

const ProfileTranslationHistory = ({ translations }) => {
    
    const max=translations.length

    const min = (translations.length > 10)? translations.length-10 : 0

    const translationList = translations.slice(min,max).map((translation, index) => <ProfileTranslationHistoryItem key={ index } translation={ translation } />)
    
    return (
        <div className="history-container">
            <h4>Your translation history</h4>
            <ul>{ translationList }</ul>
            <ProfileActions />
        </div>

    )
}
export default ProfileTranslationHistory
