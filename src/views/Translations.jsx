import TranslationsForm from "../components/Translations/TranslationsForm"
import withAuth from "../hoc/withAuth"
import { useUser } from "../context/UserContext"
import { translationAdd } from "../api/translation"
import { storageSave } from "../utils/storage"
import { STORAGE_KEY_USER } from "../const/storageKeys"
import ImgForm from "../components/Translations/ImgForm"
import { useState } from "react"

const Translations = () => {

    const { user, setUser } = useUser()

    const [ myText, setMyText ] = useState("")



    const handleTranslateClicked = async (text) => {
        console.log(text.trim())

        setMyText(text)

        const [ error, updatedUser] = await translationAdd(user, text.trim())
        if (error !==null){

            return
        }

        // KEEP UI state and Server state in sync
        storageSave(STORAGE_KEY_USER, updatedUser)
        // update context state      
        setUser(updatedUser)

        console.log('Error', error)
        console.log('Result', updatedUser)
        
    }

    return (
        <>
            <section id="text-to-translate">
                <TranslationsForm onTranslate = { handleTranslateClicked } />
            </section>
            <div className= "text-translated">
              <ImgForm myText={ myText } />
            </div>
        </>
    )
}
export default withAuth(Translations)