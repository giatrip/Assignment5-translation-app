import LoginForm from "../components/Login/LoginForm"
const Login = () => {
    return (
        <>
            <div className="line-1"></div>
            <div className="container">
                <img src="Logo.png" className="login-image"></img>
                <div className="greeting">
                    <h1 className="text">Lost in Translation</h1>
                    <h2 className="text text2">Get started</h2>
                </div>
            </div>
                <LoginForm />
            
        </>
    )
}
export default Login